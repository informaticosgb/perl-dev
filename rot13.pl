use strict;
use warnings;
use Crypt::Rot13;
use IO::File;

sub rot13 {
  my ($file, $flag) = @ARGV;
  my $rot13 = new Crypt::Rot13;
  if ($flag eq 1) {
    # print "encrypt";
    # open my $fh , '<', $file or die $!;
    # open my $fh , '>', $file or die $!;
    # open(F, $file) or die $!;
    # print <F>; # print file contents
    my $fh = new IO::File;
    my $content;
    if ($fh->open("< $file")) {
      local $/;
      $content = <$fh>;
    }

    $fh = new IO::File "> $file";
    if (defined $fh) {
      # print $fh "bar\n";
      # $fh->close;
      $rot13->charge($content);
      print $rot13->rot13();
      $fh->close;
      # print $content;
    }

  } else {
    print "decrypt";
  }

}

rot13();
