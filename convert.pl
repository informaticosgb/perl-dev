use strict;
use warnings;
use Try::Tiny;
use Scalar::Util qw(reftype);
use Switch;

sub convert {
  my ($first, $second, $third) = @ARGV;
  try {
    # && ref($second) eq "string"

    if ($first >= 2 && $first <= 25 && $third >= 2 && $third <= 25) {
      switch ($first) {
        case 2 {
          print "binario \n";
          foreach ($second) {
            # print "$_\n";
            # $_ =~ /^[0-1]+$/
            if (index($second, '0') != -1 || index($second, '1') != -1) {
              print "perro";
            } else {
              print "gato";
            }
          }
        }
        case 10 {
          print "decimal";
          foreach ($second) {
            # print "$_\n";
            # $_ =~ /^[0-1]+$/
            if ($_ =~ /^[0-9]+$/) {
              print "bien";
            } else {
              print "mal";
            }
          }

        }
      }

      switch ($third) {
        case 2 { print "binario" }
        case 10 { print "decimal" }
      }

    } else {
      print "err";
    }

  } catch {
    warn "caught error: " . $_; # not $@
  };
}

convert();
