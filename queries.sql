create database eplanning;

show databases;

use eplanning;

show tables;

-- create table if not exists customers (
-- 	_id integer unsigned not null auto_increment,
--  firstName varchar(50) not null,
-- 	lastName varchar(50) not null,
-- 	primary key (_id)
-- );

-- create table if not exists invoices (
-- 	_id integer unsigned not null auto_increment,
-- 	details varchar(50) not null,
-- 	category varchar(3) not null,
-- 	primary key (_id)
-- )

-- create table if not exists products (
-- 	_id integer unsigned not null auto_increment,
--  productName varchar(50) not null,
--  _sku varchar(20) not null,
--  details varchar(50) not null,
-- 	primary key (_id)
-- )

-- alter table customers drop column date;
-- alter table customers add date date not null;
-- alter table customers add balance varchar(20) not null;
-- alter table customers add debtor boolean default false;

-- select CURDATE();
truncate table customers;
select * from customers;

insert into customers values (null, 'Gerson', 'Sanchez', CURDATE(), 1000, true);

-- Realizar una consulta SQL que permita determinar el saldo de un cliente a la fecha de hoy.
select _id, firstName, lastName, balance from customers where firstName = 'Gerson' and date = CURDATE();

-- Realizar una consulta SQL que retorne todos los clientes deudores a la fecha de hoy.
select _id, firstName, lastName from customers where debtor = 1 and date = CURDATE();

-- drop database eplannig;
