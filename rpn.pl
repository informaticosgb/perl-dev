use strict;
use warnings;
use Data::Dumper qw(Dumper);
use Scalar::Util qw(looks_like_number);
use Switch;

sub rpn {
  my ($exp) = @ARGV;
  my @spl = split / /, $exp;
  my @result = ();
  foreach (@spl) {
    my $isnum = looks_like_number($_);
    if ($isnum) {
      push(@result, $_);
    }

    switch ($_) {
      case "+" {
        my $sum = @result[scalar(@result - 2)] + @result[scalar(@result - 1)];
        splice @result, scalar(@result - 2), 2;
        push(@result, $sum);
      }
      case "-" {
        my $sub = @result[scalar(@result - 2)] - @result[scalar(@result - 1)];
        splice @result, scalar(@result - 2), 2;
        push(@result, $sub);
      }
      case "*" {
        my $mul = @result[scalar(@result - 2)] * @result[scalar(@result - 1)];
        splice @result, scalar(@result - 2), 2;
        push(@result, $mul);
      }
      # case "x" {
      #   my $mul = @result[scalar(@result - 2)] * @result[scalar(@result - 1)];
      #   splice @result, scalar(@result - 2), 2;
      #   push(@result, $mul);
      # }
      case "/" {
        my $div = @result[scalar(@result - 2)] / @result[scalar(@result - 1)];
        splice @result, scalar(@result - 2), 2;
        push(@result, $div);
      }
    }

  }

  return @result;
}

print rpn();
