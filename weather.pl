use strict;
use warnings;
use LWP::UserAgent;
use LWP::Simple;
use Data::Dumper qw(Dumper);
use JSON;

my $city = $ARGV[0];
my $contents = decode_json get("http://api.openweathermap.org/data/2.5/weather?q=" . $city . ",ar&appid=8883dea85d18c78944d9504572025ce1");
print $contents->{weather}->[0]->{main};
